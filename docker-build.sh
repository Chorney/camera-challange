#!/bin/bash
docker build -t drchorney/api:latest ./api/
docker build -t drchorney/data-pull:latest ./data-pull/
docker build -t drchorney/db:latest ./db/
docker build -t drchorney/mock-endpoint:latest ./mock-endpoint/
docker build -t drchorney/proxy:latest ./proxy/

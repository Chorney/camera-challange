package main

import (
	"fmt"
	"log"
	"net/http"

	"gitlab.com/camera/api/config"
	"gitlab.com/camera/api/httphandlers"

	"github.com/gorilla/mux"
)

func main() {

	fmt.Println("Production Mode: ", config.Options.ProductionFlag)
	fmt.Println("Configuration file: ", config.Config.Server)

	err := http.ListenAndServe(":8085", handlers())
	if err != nil {
		log.Fatal(err)
	}
}

func handlers() http.Handler {
	hc := httphandlers.NewCameraHandler()
	hi := httphandlers.NewImageHandler()

	r := mux.NewRouter()
	r.HandleFunc("/api/helloworld", hc.HelloWorld).Methods("GET")

	//camera endpoint
	r.HandleFunc("/camera", MakeHandler(hc.GetCamera)).Methods("GET")
	r.HandleFunc("/camera/{id}", MakeHandler(hc.ShowCamera)).Methods("GET")
	r.HandleFunc("/camera", hc.PostCamera).Methods("POST")

	//image endpoint
	r.HandleFunc("/image", MakeHandler(hi.GetImage)).Methods("GET")
	r.HandleFunc("/image", hi.PostImage).Methods("POST")

	return r
}

//MakeHandler set's headers when wrapped around controller functions
func MakeHandler(fn func(w http.ResponseWriter, r *http.Request)) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		if config.Options.ProductionFlag == "false" {
			w.Header().Set("Access-Control-Allow-Origin", "*")
		} else {
			origin := r.Header.Get("Origin")
			fmt.Println(origin)
			w.Header().Set("Access-Control-Allow-Origin", origin)
		}

		w.Header().Set("Content-Type", "application/json")
		fn(w, r)
	}
}

// func OptionsHandler(w http.ResponseWriter, r *http.Request) {
// 	if origin := r.Header.Get("Origin"); origin != "" {
// 		w.Header().Set("Access-Control-Allow-Origin", origin)
// 		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
// 		w.Header().Set("Access-Control-Allow-Headers",
// 			"Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
// 	}
// }

// func MakeUserLockedHandler(fn func(w http.ResponseWriter, r *http.Request)) func(w http.ResponseWriter, r *http.Request) {
// 	return func(w http.ResponseWriter, r *http.Request) {
// 		if config.Options.UnlockedMode == "false" {
// 			status, _ := controllers.AlreadyLoggedIn(r)
// 			if status == false {
// 				w.WriteHeader(http.StatusForbidden)
// 				return
// 			}
// 		}
// 		fn(w, r)
// 	}
// }

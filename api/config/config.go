package config

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"sync"
)

var Options FlagConfig

type FlagConfig struct {
	ProductionFlag string
	DebugMode      string
	Verbosity      int
	DecLocMsg      string
	FullErrMsg     string
}

var Config Configuration
var Once sync.Once

type Configuration struct {
	Server string
}

func init() {
	processFlags()
	Once.Do(func() {
		cfg := ParseConfig()
		Config = cfg
	})
}

func ParseConfig() Configuration {
	var err error
	var raw []byte

	c := Configuration{}
	if Options.ProductionFlag == "true" {
		c.Server = os.Getenv("GO_SERVER_NAME")
	} else {
		raw, err = ioutil.ReadFile("config.json")
		if err != nil {
			fmt.Println(err.Error())
			os.Exit(1)
		}
		json.Unmarshal(raw, &c)
	}

	return c
}

func processFlags() {
	flag.StringVar(&Options.ProductionFlag, "production", "false", "When in production mode uses GO_SERVER_NAME and GO_DBNAME environment variables")
	flag.Parse()
}

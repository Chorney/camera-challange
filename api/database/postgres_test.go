package database

import (
	"database/sql"
	"fmt"
	"testing"
)

const serverName = "user=postgres host=localhost dbname=osprey port=5435 sslmode=disable"

func TestConnection(t *testing.T) {

	// serverName := "postgres://postgres@opsrey:5435?sslmode=disable"

	_ = NewSqlDb(serverName)

}

func TestGetCamerasLimit(t *testing.T) {

	db := NewSqlDb(serverName)
	defer db.db.Close()

	tests := []struct {
		limit    int
		expected int
	}{
		{0, 5},
		{1, 1},
		{3, 3},
	}

	for _, tt := range tests {
		rows := db.GetCameras(tt.limit)

		counter := 0
		for rows.Next() {
			counter++
		}

		if counter != tt.expected {
			t.Fatalf("tests - counter wrong. expected=%d, got %d", tt.expected, counter)
		}
	}

}

type row struct {
	id    int
	name  string
	total int
}

func TestGetCamerasByFileSize(t *testing.T) {

	db := NewSqlDb(serverName)
	defer db.db.Close()

	var sds SortDataSize = 0
	var id int
	var total int
	var name string
	var m map[int]row
	m = make(map[int]row)

	// test values I've manually inputed knowing what I have in test DB
	m[1] = row{1, "Area-1-NW", 13000}
	m[2] = row{2, "Area-2-NW", 6000}
	m[3] = row{3, "Area-3-NW", 500}

	rows := db.GetCameras(0, sds)

	counter := 0
	for rows.Next() {
		switch err := rows.Scan(&name, &id, &total); err {
		case sql.ErrNoRows:
			t.Errorf("No rows were returned!")
		case nil:
			fmt.Println(id, total, name)
			if m[id].id != id || m[id].total != total || m[id].name != name {
				t.Errorf("expected id: %d, total: %d, name: %s, got %d, %d, %s, respetively", m[id].id, m[id].total,
					m[id].name, id, total, name)
			}

		default:
			panic(err)
		}

		counter++
	}

	if counter != 3 {
		t.Fatalf("tests - counter wrong. expected=%d, got %d", 3, counter)
	}

}

func TestGetCameraByCount(t *testing.T) {

	db := NewSqlDb(serverName)
	defer db.db.Close()

	var sni SortNumImages = 0 // ascending
	var id int
	var count int
	var name string
	var m map[int]row
	m = make(map[int]row)

	// test values I've manually inputed knowing what I have in test DB
	m[1] = row{1, "Area-1-NW", 4}
	m[2] = row{2, "Area-2-NW", 2}
	m[3] = row{3, "Area-3-NW", 1}

	rows := db.GetCameras(0, sni)

	counter := 0
	for rows.Next() {
		switch err := rows.Scan(&name, &id, &count); err {
		case sql.ErrNoRows:
			t.Errorf("No rows were returned!")
		case nil:
			fmt.Println(id, count, name)
			if m[id].id != id || m[id].total != count || m[id].name != name {
				t.Errorf("expected id: %d, count: %d, name: %s, got %d, %d, %s, respetively", m[id].id, m[id].total,
					m[id].name, id, count, name)
			}

		default:
			panic(err)
		}

		counter++
	}

	if counter != 3 {
		t.Fatalf("tests - counter wrong. expected=%d, got %d", 3, counter)
	}

}

func TestGetImagesLimit(t *testing.T) {

	db := NewSqlDb(serverName)
	defer db.db.Close()

	tests := []struct {
		limit    int
		expected int
	}{
		{0, 7},
		{1, 1},
		{3, 3},
	}

	for _, tt := range tests {
		rows := db.GetImages(tt.limit)

		counter := 0
		for rows.Next() {
			counter++
		}

		if counter != tt.expected {
			t.Fatalf("tests - counter wrong. expected=%d, got %d", tt.expected, counter)
		}
	}
}

func TestFindCamera(t *testing.T) {
	db := NewSqlDb(serverName)
	defer db.db.Close()

	err := db.FindCamera(1)
	if err != nil {
		t.Errorf("expected to find camera of id 1")
	}

	err = db.FindCamera(1000)
	if err == nil {
		t.Errorf("expected to not find any cameras")
	}
}

type image struct {
	cameraId  int
	id        int
	timestamp int
	max       int
}

func TestGetMaxImages(t *testing.T) {

	db := NewSqlDb(serverName)
	defer db.db.Close()

	var slic SortLargestImagesCamera = 0
	var id int
	var max int
	var cameraId int
	var timestamp int
	var m map[int]image
	m = make(map[int]image)

	// test values I've manually inputed knowing what I have in test DB
	m[1] = image{1, 4, 20000, 7000}
	m[2] = image{2, 6, 20000, 5000}
	m[3] = image{3, 7, 20000, 500}

	rows := db.GetImages(0, slic)

	cols, _ := rows.Columns()
	fmt.Printf("%v", cols)

	counter := 0
	for rows.Next() {
		switch err := rows.Scan(&id, &cameraId, &max, &timestamp); err {
		case sql.ErrNoRows:
			t.Errorf("No rows were returned!")
		case nil:
			fmt.Println(id, max, cameraId)
			if m[cameraId].id != id || m[cameraId].max != max || m[cameraId].cameraId != cameraId {
				t.Errorf("expected id: %d, max: %d, cameraId: %d, got %d, %d, %d, respetively", m[cameraId].id,
					m[cameraId].max, m[cameraId].cameraId, id, max, cameraId)
			}
		default:
			panic(err)
		}

		counter++
	}

	if counter != 3 {
		t.Fatalf("tests - counter wrong. expected=%d, got %d", 3, counter)
	}

}

func TestImageTimeStampSort(t *testing.T) {

	db := NewSqlDb(serverName)
	defer db.db.Close()

	var st SortTimestamp = 0 // ascending
	var id int
	var timestamp int
	var fileSize int
	var cameraId int

	// test values I've manually inputed knowing what I have in test DB
	rows := db.GetImages(0, st)
	rows.Next()
	rows.Scan(&id, &cameraId, &fileSize, &timestamp)

	if timestamp != 20000 {
		t.Errorf("expect timestamp to be 20000, got %d", timestamp)
	}

	st = 1
	rows = db.GetImages(0, st)
	rows.Next()
	rows.Scan(&id, &cameraId, &fileSize, &timestamp)

	if timestamp != 23000 {
		t.Errorf("expect timestamp to be 23000, got %d", timestamp)
	}
}

func TestCameraInsert(t *testing.T) {

	db := NewSqlDb(serverName)
	defer db.db.Close()

	cname := "camera123"

	_, err := db.InsertCamera(cname)
	if err != nil {
		t.Errorf("Error inserting data into the database")
	}

	// check to see if the inserted row is in the table
	rows, err := db.db.Query(fmt.Sprintf(`SELECT * FROM cameras WHERE name = '%s'`, cname))
	if err != nil {
		fmt.Printf("%v", err)
		t.Errorf("Error retrevigin document from database")
	}

	counter := 0
	for rows.Next() {
		counter++
	}

	if counter != 1 {
		t.Errorf("More than 1 or none found in database")
	}

	//clean up the insert
	sqlStatement := fmt.Sprintf(`DELETE FROM cameras where name='%s';`, cname)
	_, err = db.db.Exec(sqlStatement)
	if err != nil {
		t.Errorf("Trouble cleaning up the database")
	}
}

func TestGetMostRecentImage(t *testing.T) {
	db := NewSqlDb(serverName)
	defer db.db.Close()

	timestamp := db.GetMostRecentImage(1)

	if timestamp != 23000 {
		t.Errorf("Expected 23000, got %d", timestamp)
	}

	timestamp = db.GetMostRecentImage(3)
	if timestamp != 20000 {
		t.Errorf("Expected 20000, got %d", timestamp)
	}
}
func TestImageInsert(t *testing.T) {

	db := NewSqlDb(serverName)
	defer db.db.Close()

	cid := 1
	fileSize := 1700
	timestamp := 200000

	_, err := db.InsertImage(cid, fileSize, timestamp)
	if err != nil {
		t.Errorf("Error inserting data into the database")
	}

	// check to see if the inserted row is in the table
	rows, err := db.db.Query(fmt.Sprintf(`SELECT * FROM images WHERE camera_id = %d AND file_size = %d;`, cid, fileSize))
	if err != nil {
		fmt.Printf("%v", err)
		t.Errorf("Error retrevigin document from database")
	}

	counter := 0
	for rows.Next() {
		counter++
	}

	if counter != 1 {
		t.Errorf("More than 1 or none found in database")
	}

	//clean up the insert
	sqlStatement := fmt.Sprintf(`DELETE FROM images WHERE camera_id = %d AND file_size = %d`, cid, fileSize)
	_, err = db.db.Exec(sqlStatement)
	if err != nil {
		t.Errorf("Trouble cleaning up the database")
	}
}

package database

import (
	"database/sql"
	"fmt"
	"log"
	"strconv"

	_ "github.com/lib/pq"
)

//SortDataSize query database paramater flags for the camera table
type SortDataSize int

//SortNumImages query database paramater flags for the camera table
type SortNumImages int

//SortLargestImagesCamera query database parameter flags for the images table
type SortLargestImagesCamera int

type SortTimestamp int

type SqlDb struct {
	db *sql.DB
}

func NewSqlDb(server string) *SqlDb {
	db_ := SqlDb{}
	db_.ConnectDb(server)
	return &db_
}

func (sqldb *SqlDb) ConnectDb(server string) {

	DB, err := sql.Open("postgres", server)

	if err != nil {
		log.Fatalln(err)
	}

	if err = DB.Ping(); err != nil {
		log.Fatalln(err)
		// msg := fmt.Sprintf("Error finding elements in the database")
		// logging.Error(msg, "-postgres.go-ConnectDb", err)
	}
	fmt.Println("You connected to your local POSTGRESS database")

	sqldb.db = DB
}

func (sqldb *SqlDb) Close() {
	sqldb.db.Close()
}

//GetCameras handles the database query to grab camera data depending on query parameters
func (sqlDb *SqlDb) GetCameras(limit int, args ...interface{}) *sql.Rows {

	var q string

	if len(args) == 0 {
		q = `SELECT * FROM cameras`
	} else {
		q = `SELECT c.name, camera_id, `
	}

	for _, arg := range args {
		switch arg.(type) {
		case SortNumImages:
			q = q + ` COUNT(i.file_size) AS count from cameras c INNER JOIN images i ON 
			c.id = i.camera_id GROUP BY camera_id, c.name ORDER BY count `
			if arg.(SortNumImages) == 0 {
				q = q + `ASC`
			} else {
				q = q + `DESC`
			}
		case SortDataSize:
			q = q + ` SUM(i.file_size) AS total FROM cameras c INNER JOIN images i ON
			c.id = i.camera_id GROUP BY camera_id, c.name ORDER BY total `
			if arg.(SortDataSize) == 0 {
				q = q + `ASC`
			} else {
				q = q + `DESC`
			}
		default:
			panic("Unknown argument")
		}
	}

	if limit > 0 {
		q = q + " LIMIT " + strconv.Itoa(limit)
	}

	q = q + `;`

	fmt.Printf("%s\n", q)

	rows, err := sqlDb.db.Query(q)
	if err != nil {
		fmt.Println("Error querying database")
		log.Fatalln(err)
	}

	return rows
}

//FindCamera searches cameras table for camera with given id
func (sqlDb *SqlDb) FindCamera(id int) error {
	var returnedId int
	sqlStatement := `SELECT id FROM cameras WHERE id=$1;`
	row := sqlDb.db.QueryRow(sqlStatement, id)
	err := row.Scan(&returnedId)
	if err != nil {
		fmt.Printf("Error finding row in table: %v", err)
		return err
	}
	return nil
}

//InsertCamera inserts a camera document into database
func (sqlDb *SqlDb) InsertCamera(name string) (int, error) {

	var id int
	sqlStatement := fmt.Sprintf(` INSERT INTO cameras (name)
										VALUES ('%s') RETURNING id`, name)
	err := sqlDb.db.QueryRow(sqlStatement).Scan(&id)
	if err != nil {
		fmt.Printf("Error executing insert: %v", err)
		return 0, err
	}

	return id, nil
}

//GetImages retreives images from the database depending on quey parameters
func (sqlDb *SqlDb) GetImages(limit int, args ...interface{}) *sql.Rows {

	var q string

	// if no query parameters ( not including limit ) then just do default select
	if len(args) == 0 {
		q = `SELECT * FROM images`
	} else {
		q = ``
	}

	for _, arg := range args {
		switch arg.(type) {
		case SortLargestImagesCamera:
			q = q + `SELECT images.id, images.camera_id, images.file_size, images.timestamp FROM images INNER JOIN
				(SELECT camera_id, MAX(file_size) AS max
    		FROM images
				GROUP BY camera_id) maximages 
				ON images.camera_id = maximages.camera_id
				AND images.file_size = maximages.max ORDER BY file_size `
			if arg.(SortLargestImagesCamera) == 0 {
				q = q + `ASC`
			} else {
				q = q + `DESC`
			}
		case SortTimestamp:
			q = q + `SELECT * FROM images ORDER BY timestamp `
			if arg.(SortTimestamp) == 0 {
				q = q + `ASC`
			} else {
				q = q + `DESC`
			}
		default:
			panic("Unknown argument")
		}
	}

	if limit > 0 {
		q = q + " LIMIT " + strconv.Itoa(limit)
	}

	q = q + `;`

	fmt.Printf("%s\n", q)

	rows, err := sqlDb.db.Query(q)
	if err != nil {
		fmt.Println("Error querying database")
		log.Fatalln(err)
	}

	return rows
}

// GetMostRecentImage returns
func (sqlDb *SqlDb) GetMostRecentImage(cameraId int) int {

	var timestamp int
	sqlStatement := fmt.Sprintf(`SELECT timestamp FROM images WHERE camera_id=%d ORDER BY timestamp DESC`, cameraId)
	row := sqlDb.db.QueryRow(sqlStatement)

	row.Scan(&timestamp)
	return timestamp
}

func (sqlDb *SqlDb) InsertImage(cameraId int, fileSize int, timestamp int) (int, error) {

	var id int
	sqlStatement := fmt.Sprintf(` INSERT INTO images (camera_id, file_size, timestamp)
										VALUES ('%d','%d', '%d') RETURNING id`, cameraId, fileSize, timestamp)
	err := sqlDb.db.QueryRow(sqlStatement).Scan(&id)
	if err != nil {
		fmt.Printf("Error executing insert: %v", err)
		return 0, err
	}

	return id, nil
}

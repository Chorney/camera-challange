package httphandlers

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/camera/api/config"
	"gitlab.com/camera/api/database"
)

//CameraHandler is the instance created by the constructor. Has a number of methods to handle the
//incoming http requests
type CameraHandler struct {
}

//NewCameraHandler is the constructor for the asset controllers
func NewCameraHandler() *CameraHandler {
	return &CameraHandler{}
}

//HelloWorld is just a quick controller function to test app configuration and installation
func (ch CameraHandler) HelloWorld(w http.ResponseWriter, r *http.Request) {

	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "Hello World")
}

func (ch CameraHandler) ShowCamera(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(mux.Vars(r)["id"])
	if err != nil {
		fmt.Println("error converting string to integer")
	}

	DB := database.NewSqlDb(config.Config.Server)
	defer DB.Close()

	timestamp := DB.GetMostRecentImage(id)

	uj, _ := json.Marshal(struct {
		Timestamp int `json:"timestamp"`
	}{timestamp})

	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "%s\n", uj)
	return
}

// GetCamera GET request route camera/
func (ch CameraHandler) GetCamera(w http.ResponseWriter, r *http.Request) {

	// limit query parameter
	var limit int
	if val, ok := r.URL.Query()["limit"]; ok {
		//do something here
		limit, _ = strconv.Atoi(val[0])
	} else {
		limit = 0
	}

	sortDataSize := -1
	if val, ok := r.URL.Query()["sort_data_size"]; ok {
		sortDataSize, _ = strconv.Atoi(val[0])
	}

	sortNumImages := -1
	if val, ok := r.URL.Query()["sort_num_images"]; ok {
		sortNumImages, _ = strconv.Atoi(val[0])
	}

	DB := database.NewSqlDb(config.Config.Server)
	defer DB.Close()

	var rows *sql.Rows
	if sortNumImages == -1 && sortDataSize == -1 {
		rows = DB.GetCameras(limit)

		type Camera struct {
			CameraId int    `json:"id"`
			Name     string `json:"name"`
		}

		var output []Camera
		for rows.Next() {
			var row Camera
			err := rows.Scan(&row.CameraId, &row.Name)
			if err != nil {
				fmt.Printf("error: %s", err)
			}
			output = append(output, row)
		}

		uj, _ := json.Marshal(output)
		w.WriteHeader(http.StatusOK)
		fmt.Fprintf(w, "%s\n", uj)
		return
	} else if sortNumImages != -1 && sortDataSize != -1 {
		fmt.Printf("two of them not nil")
		w.WriteHeader(http.StatusBadRequest)
		return
	} else if sortNumImages != -1 {
		rows = DB.GetCameras(limit, database.SortNumImages(sortNumImages))

		type Camera struct {
			Name     string `json:"name"`
			CameraId int    `json:"id"`
			Count    int    `json:"count"`
		}

		var output []Camera
		for rows.Next() {
			var row Camera
			err := rows.Scan(&row.Name, &row.CameraId, &row.Count)
			if err != nil {
				fmt.Printf("error: %s", err)
			}
			output = append(output, row)
		}

		uj, _ := json.Marshal(output)
		w.WriteHeader(http.StatusOK)
		fmt.Fprintf(w, "%s\n", uj)
		return
	} else {
		rows = DB.GetCameras(limit, database.SortDataSize(sortDataSize))

		type Camera struct {
			Name     string `json:"name"`
			CameraId int    `json:"id"`
			Total    int    `json:"total"`
		}

		var output []Camera
		for rows.Next() {
			var row Camera
			err := rows.Scan(&row.Name, &row.CameraId, &row.Total)
			if err != nil {
				fmt.Printf("error: %s", err)
			}
			output = append(output, row)
		}

		uj, _ := json.Marshal(output)
		w.WriteHeader(http.StatusOK)
		fmt.Fprintf(w, "%s\n", uj)
		return
	}

}

// PostCamera POST request route camera/
func (ch CameraHandler) PostCamera(w http.ResponseWriter, r *http.Request) {

	fmt.Println("inside PostCamera")

	b, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()

	if err != nil {
		fmt.Printf("err : %s\n", err)
	}

	var m struct {
		Name string `json:"name"`
	}

	err = json.Unmarshal(b, &m)
	if err != nil {
		fmt.Printf("err : %s\n", err)
	}

	DB := database.NewSqlDb(config.Config.Server)
	defer DB.Close()

	id, err := DB.InsertCamera(m.Name)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	w.WriteHeader(http.StatusCreated)
	uj, _ := json.Marshal(struct {
		Name     string `json:"name"`
		CameraId int    `json:"id"`
	}{m.Name, id})
	fmt.Fprintf(w, "%s\n", uj)
	return
}

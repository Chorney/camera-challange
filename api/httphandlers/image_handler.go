package httphandlers

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"gitlab.com/camera/api/config"
	"gitlab.com/camera/api/database"
)

//ImageHandler is the struct for attaching the restful routes for image handling
type ImageHandler struct {
}

//NewImageHandler is the constructor for the Image routes
func NewImageHandler() *ImageHandler {
	return &ImageHandler{}
}

//GetImage is the GET request for route /image
func (ih ImageHandler) GetImage(w http.ResponseWriter, r *http.Request) {

	// limit query parameter
	var limit int
	if val, ok := r.URL.Query()["limit"]; ok {
		//do something here
		limit, _ = strconv.Atoi(val[0])
	} else {
		limit = 0
	}

	largetImagesCamera := -1
	if val, ok := r.URL.Query()["larget_images_camera"]; ok {
		largetImagesCamera, _ = strconv.Atoi(val[0])
	}

	DB := database.NewSqlDb(config.Config.Server)
	defer DB.Close()

	var rows *sql.Rows
	if largetImagesCamera == -1 {
		rows = DB.GetImages(limit)

		type Image struct {
			FileSize  int `json:"file_size`
			CameraId  int `json:"camera_id"`
			Id        int `json:"id"`
			Timestamp int `json:"timestamp"`
		}

		var output []Image
		for rows.Next() {
			var row Image
			err := rows.Scan(&row.Id, &row.CameraId, &row.FileSize, &row.Timestamp)
			if err != nil {
				fmt.Printf("error: %s", err)
			}
			output = append(output, row)
		}

		uj, _ := json.Marshal(output)
		w.WriteHeader(http.StatusOK)
		fmt.Fprintf(w, "%s\n", uj)
		return
	} else {
		rows = DB.GetImages(limit, database.SortLargestImagesCamera(largetImagesCamera))

		cols, _ := rows.Columns()
		fmt.Printf("Cols: %v", cols)

		type Image struct {
			CameraId  int `json:"camera_id"`
			Id        int `json:"id"`
			Max       int `json:"file_size"`
			Timestamp int `json:"timestamp"`
		}

		var output []Image
		for rows.Next() {
			var row Image
			err := rows.Scan(&row.Id, &row.CameraId, &row.Max, &row.Timestamp)
			if err != nil {
				fmt.Printf("error: %s", err)
			}
			output = append(output, row)
		}

		uj, _ := json.Marshal(output)
		w.WriteHeader(http.StatusOK)
		fmt.Fprintf(w, "%s\n", uj)
		return
	}

}

// PostImage POST request route image/
func (ih ImageHandler) PostImage(w http.ResponseWriter, r *http.Request) {

	b, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()

	if err != nil {
		fmt.Printf("err : %s\n", err)
	}

	var m struct {
		CameraId  int `json:"camera_id"`
		FileSize  int `json:"file_size"`
		Timestamp int `json: "timestamp"`
	}

	err = json.Unmarshal(b, &m)
	if err != nil {
		fmt.Printf("err : %s\n", err)
	}

	DB := database.NewSqlDb(config.Config.Server)
	defer DB.Close()

	err = DB.FindCamera(m.CameraId)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "No camera id in database")
		return
	}

	id, err := DB.InsertImage(m.CameraId, m.FileSize, m.Timestamp)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	w.WriteHeader(http.StatusCreated)
	uj, _ := json.Marshal(struct {
		Id        int `json:"id"`
		CameraId  int `json:"camera_id"`
		FileSize  int `json:"file_size"`
		TimeStamp int `json:"timestamp"`
	}{id, m.CameraId, m.FileSize, m.Timestamp})
	fmt.Fprintf(w, "%s\n", uj)
	return
}

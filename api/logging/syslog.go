package logging

import (
	"fmt"
	"log"

	"gitlab.com/chorney/force-plots/config"
)

//DecorateMessageLoc is a message decorator to add the code location of the error message
func DecorateMessageLoc(msg string, loc string) string {
	if config.Options.DecLocMsg == "true" {
		return fmt.Sprintf("%v : %v", loc, msg)
	}
	return msg
}

//DecorateMessageErr adds the golang error message to the message
func DecorateMessageErr(msg string, err error) string {
	if config.Options.FullErrMsg == "true" {
		msg = fmt.Sprintf("%v : error - %v", msg, err)
	}
	return msg
}

//Emergency message, verbose = 0 , syslog priority EMERG
func Emergency(msg string, loc string, err error) {
	msg = DecorateMessageLoc(msg, loc)
	msg = DecorateMessageErr(msg, err)
	if config.Options.Verbosity >= 0 {
		log.Fatal(msg)
	}
}

// Alert message, verbose = 3, syslog priority ALERT
func Alert(msg string, loc string) {
	msg = DecorateMessageLoc(msg, loc)
	if config.Options.Verbosity >= 3 {
		fmt.Println(msg)
	}
}

// Critical message, verbose = 1, syslog priority CRIT
func Critical(msg string, loc string, err error) {
	msg = DecorateMessageLoc(msg, loc)
	msg = DecorateMessageErr(msg, err)
	if config.Options.Verbosity >= 0 {
		log.Fatal(msg)
	}
}

// Error message, verbose = 2, syslog priority ERR
func Error(msg string, loc string, err error) {
	msg = DecorateMessageLoc(msg, loc)
	msg = DecorateMessageErr(msg, err)
	if config.Options.Verbosity >= 1 {
		fmt.Println(msg)
	}
}

//Warning message verbose =2, syslog priority WARNING
func Warning(msg string, loc string) {
	msg = DecorateMessageLoc(msg, loc)
	if config.Options.Verbosity >= 2 {
		fmt.Println(msg)
	}
}

//Notice message verbose = 3, syslog priority NOTICE
func Notice(msg string, loc string) {
	msg = DecorateMessageLoc(msg, loc)
	if config.Options.Verbosity >= 3 {
		fmt.Println(msg)
	}
}

//Info notice, verbose = 4, syslog priority INFO
func Info(msg string, loc string) {
	msg = DecorateMessageLoc(msg, loc)
	if config.Options.Verbosity >= 4 {
		fmt.Println(msg)
	}
}

//Message is a general message to output, verbosity level 5
func Message(msg string, loc string) {
	msg = DecorateMessageLoc(msg, loc)
	if config.Options.Verbosity >= 5 {
		fmt.Println(msg)
	}
}

//DebugMessage is a message that prints to stdout if in Debug mode.
func DebugMessage(msg string, loc string) {
	msg = DecorateMessageLoc(msg, loc)
	if config.Options.DebugMode == "true" {
		fmt.Println(msg)
	}
}

package helpers

import (
	"reflect"

	"gitlab.com/chorney/force-plots/database"

	"gopkg.in/mgo.v2/bson"
)

//InArray  v in in
func InArray(v interface{}, in interface{}) (ok bool, i int) {
	val := reflect.Indirect(reflect.ValueOf(in))
	switch val.Kind() {
	case reflect.Slice, reflect.Array:
		for ; i < val.Len(); i++ {
			if ok = v == val.Index(i).Interface(); ok {
				return
			}
		}
	}
	return
}

//ListModelsToIds takes a list of models and then outputs a slice of the bson.ObjectIds
func ListModelsToIds(models interface{}) []bson.ObjectId {
	val := reflect.Indirect(reflect.ValueOf(models))

	ids := []bson.ObjectId{}

	switch val.Kind() {
	case reflect.Slice, reflect.Array:
		for i := 0; i < val.Len(); i++ {
			model := val.Index(i).Interface().(database.Model)
			ids = append(ids, model.GetModelID())
		}
	}
	return ids
}

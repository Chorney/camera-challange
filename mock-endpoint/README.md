# Mock Server
* this is the file for creating the mock server off of data.json
* Mock server is created using node.js json-server
* The python data-pull service will poll this mock server every 60 seconds to check for new images

## installation
npm install -g json-server

## Run endpoin:
json-server --watch data.json

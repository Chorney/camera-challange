import requests
import json
import pdb

TIMEOUTVALUE = 10

def getJson(url):
  data = requests.get(url, timeout=TIMEOUTVALUE)
  jsonData = json.loads(data.text)
  return jsonData

#checkNewData given an id checks for a new id
def checkForNewDataFromCameraId(id, cameraApi, endpointApi):
  try:
    print(cameraApi+"/camera/"+str(id))
    data = getJson(cameraApi+"/camera/"+str(id))
    print(data)
    timestamp = data['timestamp']
    print(timestamp)

    data = getJson(endpointApi+"/cameras/"+str(id))
    print(endpointApi)
    images = data['images']
    print(images)
    for image in images:
      its = image['timestamp']
      if its > timestamp:
        res = requests.post(cameraApi+"/image",json={"camera_id": id, "file_size": image['file_size'], "timestamp": its}, timeout=TIMEOUTVALUE)
  except requests.exceptions.ConnectionError:
    print("connection error")
  except:
    print("default error")
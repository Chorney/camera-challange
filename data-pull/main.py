import time
import pdb
import helpers
import sys
import json
import os

try:
  arg1 = sys.argv[1]
  print(arg1)
  DEVELOPMENT = True
except:
  DEVELOPMENT = False
  pass

with open("config.json") as f:
  data = json.load(f)
  if DEVELOPMENT == True:
    cameraApi = data['camera_api']
    endpointApi = data['endpoint_api']
  elif DEVELOPMENT == False:
    cameraApi = os.environ['CAMERA_API']
    endpointApi = os.environ['ENDPOINT_API']

cameraArray = [1,2,3,4,5,6] # N cameras

while True:
  for id in cameraArray:
    helpers.checkForNewDataFromCameraId(id,cameraApi, endpointApi)
  time.sleep(60)








# Camera Endpoint
* This is a restful endpoint for cameras and images


## API
* [swagger api](https://app.swaggerhub.com/apis/drchorney/camera-challenge/1.0.0)

## Tech Stack - 5 docker containers
* Golang endpointi - api
* Postgres database - db
* Nignx proxy - proxy
* python pull - data-pull
* mock json-server endpoint - mock-endpoint

* Note: 5 docker containeres built locally and pushed to dockerhub, docker-compose of remote AWS server pulls down containers and deploys on a single AWS ubuntu server

### Osprey Code Challenge:

We have N cameras. Each has an API endpoint at which a list of images
taken by the camera is available.


domain.com/camera/<camera_id>/


A get request to the above endpoint returns data of the following
structure
{
camera_id: 1,
images: [
{
file_size: 42048,
},
{
file_size: 1024,
},
…
]
}


Given the nature of the camera deployments, requests to some cameras may
time out or experience connection errors. We would like to set the
timeout as a parameter and any inability to retrieve data should be
handled gracefully.


We would like to be able to answer some simple questions:


- Which cameras have used the most data?
- Which cameras have the highest number of images?
- What are the largest images per camera?


Please do your best to consider performance, reusability and legibility
in your implementation. Assume that the solution might be used on
datasets up to 10 million images. Additionally, assume that this code
will be going into production.


** Please provide unit tests to confirm your solution works and note
that there is no live endpoint available for testing. **


### Solution:

* Python function pulls the camera endpoints, and then posts new data to a remote Golang endpoint, query parameters on camera/ and image/ allows user to run the above questions. Golang endpoint talks to a postgres database.

* golang endpoint published at:

http://ec2-34-218-44-194.us-west-2.compute.amazonaws.com/api/camera/1

* json-server mock endpoint at: 

http://ec2-34-218-44-194.us-west-2.compute.amazonaws.com/remote_camera/

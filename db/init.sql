CREATE DATABASE osprey
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    CONNECTION LIMIT = -1;

\connect osprey

/*
CREATE THE CAMERAS TABLE
*/

CREATE TABLE cameras (
	id SERIAL PRIMARY KEY,
	name VARCHAR(20) NOT NULL
);

/*
CREATE THE IMAGES TABLE
*/ 

CREATE TABLE images (
	id SERIAL PRIMARY KEY, 
	camera_id INT REFERENCES cameras(id),
	file_size INT,
	timestamp INT
);

/* 
INSERTING OUR DATA INTO THE CAMERAS TABLE
*/ 

INSERT INTO cameras (name)
VALUES ('Area-1-NW');

INSERT INTO cameras (name)
VALUES ('Area-2-NW');

INSERT INTO cameras (name)
VALUES ('Area-3-NW');

INSERT INTO cameras (name)
VALUES ('Area-4-NW');

INSERT INTO cameras (name)
VALUES ('Area-5-NW');

INSERT INTO cameras (name)
VALUES ('Area-6-SW');

/*
INSERTING OUR DATA INTO THE EMPLOYEES TABLE
*/ 

INSERT INTO images (camera_id, file_size, timestamp)
VALUES (1, 1000, 20000);

INSERT INTO images (camera_id, file_size, timestamp)
VALUES (1, 2000, 21000);

INSERT INTO images (camera_id, file_size, timestamp)
VALUES (1, 3000, 22000);

INSERT INTO images (camera_id, file_size, timestamp)
VALUES (1, 7000, 23000);

INSERT INTO images (camera_id, file_size, timestamp)
VALUES (2, 1000, 20000);

INSERT INTO images (camera_id, file_size, timestamp)
VALUES (2, 5000, 21000);

INSERT INTO images (camera_id, file_size, timestamp)
VALUES (3, 500, 20000);

INSERT INTO images (camera_id, file_size, timestamp)
VALUES (4, 500, 20000);

INSERT INTO images (camera_id, file_size, timestamp)
VALUES (4, 557, 20000);

INSERT INTO images (camera_id, file_size, timestamp)
VALUES (5, 19557, 21000);
